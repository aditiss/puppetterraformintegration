provider "aws" {
  region = "us-east-1"
}

# Use existing VPC
data "aws_vpc" "existing_vpc" {
  id = "vpc-0ad19c8dd924fb26e"
}

# Use existing subnet
data "aws_subnet" "existing_subnet" {
  vpc_id = data.aws_vpc.existing_vpc.id
  id     = "subnet-03eeb62196cc4c723"
}

# Use existing security group
data "aws_security_group" "existing_security_group" {
  id = "sg-0e9e4d0d16a21f27b"

  
}

resource "aws_instance" "puppetMaster" {
  ami                         = "ami-07d9b9ddc6cd8dd30"
  instance_type               = "t2.medium"
  subnet_id                   = data.aws_subnet.existing_subnet.id
  key_name                    = "demokey"
  associate_public_ip_address = true
  security_groups             = [data.aws_security_group.existing_security_group.id] // Attach security group to instance

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:\\JavaCode\\demokey.pem")
    host        = aws_instance.puppetMaster.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver"
    ]
  }

  tags = {
    Name = "puppetMaster"
  }
}